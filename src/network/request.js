import axios from 'axios'

export function request(config) {
  const instance = axios.create({
    baseURL: 'http://123.207.32.32:8000',
    timeout: 5000
  })

  instance.interceptors.request.use(config => {
    // 1.config中一些不符合服务器的要求
    // 2.每次发送网络请求时，在界面中显示一个请求图标
    // 3.某些网络请求，必须携带一些特殊信息

    return config
  }, err => {

  })

  instance.interceptors.response.use(res =>{
    // 1.
    return res.data
  }, error => {

  })

  return instance(config)

}
